# First we import the Fabric api
from fabric.api import *

# We can then specify host(s) and run the same commands across those systems
env.user = 'root'

env.hosts = ['1.1.2.157']

sCodePath = '/Users/andrewdavies/work/'
sBranchName = 'elmer-4000-test'
sBranchPath = sCodePath + "/" + sBranchName

def status():
    run("uptime")
    run("pwd")

def hello():
    print("Hello world!")

def hello2(name="world"):
    print("Hello %s!" % name)

def mdm_():
    code_dir = sBranchPath
    with cd(code_dir):
        sBuildConfigs = run("ls -l ./buildroot/configs")
        print("Configs availabe = " + sBuildConfigs)

def git_newbranch():
    ##code_dir = sBranchPath
    print("Creating new branch " + sBranchPath)
    # branch tasks
    with cd(sCodePath):
        run("git clone -b master " + sGitPath + " " + sBranchName)
        with cd(sBranchPath):
            # run("git submodule update --init buildroot")
            # buildroot tasks
            sBuildrootPath = sBranchPath + "/buildroot"
            print("tasks in " + sBuildrootPath)
            with cd(sBuildrootPath):
                run("git checkout -b " + sBranchName)
                sBuildConfigs = run("ls -l ./configs")
                print("Configs availabe = " + sBuildConfigs)

